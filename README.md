# ZAK_Softwareentwicklungsplan

Umsetzung des Softwareentwicklungsplan

### How to build it 
```
pip install -r requirements.txt
cd src/wunddoku
python setup.py build_ext --inplace 
```
### How to run it
```
python
import src.wunddoku.startup as st
st.main()
```
### How to test it
```
cd ..
python src/test.py
```
Der test wurde in Python geschrieben für eine schneller Entwicklung damit mehr Zeit für CI übrig blieb
