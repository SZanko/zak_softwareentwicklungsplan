from distutils.core import setup
from Cython.Build import cythonize

setup(
    name='Wunddokumentation',
    ext_modules = cythonize("startup.pyx",language_level=3)
)
