#!/usr/bin/python
import numpy as np

def veraenderung(alt,neu):
    cdef float proz
    if alt!=None and alt!=0:
        proz=((neu-alt)/alt)*100
        print("Wert hat sich um " + str(proz) + " % verändert")
        return proz

def main():
    cdef int i=0
    patientenWunden = np.zeros((100, 100))
    cdef int j=0
    cdef float arztinputalt=0
    cdef float arztinput=0
    cdef bint speicherninput
    #cdef bool speicherninput
    while i<1:
        arztinput=float(input("Wunde in qcm eingeben: "))
        print("Die Wunde ist qcm groß: "+str(arztinput))
        speicherninput=bool(input("Wert speichern?"))
        if speicherninput==True:
            print("Wert gespeichert")
            speicherInputID = int(input("Bitte PatientenID im Bereich von 0 bis 99 eingeben"))
            patientenWunden[j,speicherninput] = arztinput
        else:
            print("Wert nicht gespeichert")
        datenausgeben=bool(input("Patient und Wund-Daten ausgeben?"))
        if datenausgeben==True:
            print("DatenTest")
            veraenderung(arztinputalt,arztinput)
        arztinputalt=arztinput
        j = j + 1

if __name__=="__main__":
    main()
